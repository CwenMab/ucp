# Livraison fin de sprint : 


- merger develop dans sandbox pour chacun des trois dépots (Plume, Wax, Editoria)
- ajouter un tag marquant le numéro de version livrée
- sur le serveur Sandbox (s075-2087), faire un pull des trois dépots
- interrompre et relancer le yarn server pour prendre en compte les nouveautés ->
    - tmux a # rentrer dans la console tmux
    - ctrl-d #interrompre le service
    - source config/development.env && yarn server # relancer le service
- aprés livraison mettre à jour le numéro de version dans UCP/app/app.js afin d'afficher le numéro de la version sur le point de commencer.

# Livraison de mi-sprint

- merger sandbox dans master pour chaque dépot
- sur le serveur Preprod (s075-3087), faire un pull des trois dépots
- interrompre et relancer le yarn server pour prendre en compte les nouveautés ->
    - tmux a # rentrer dans la console tmux
    - ctrl-d #interrompre le service
    - source config/development.env && yarn server # relancer le service