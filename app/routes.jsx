import React, { Fragment } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { createGlobalStyle } from 'styled-components'

// Users and Teams
// import UsersManager from 'pubsweet-component-users-manager/src/UsersManager'
import GlobalTeamsManager from 'pubsweet-component-editoria-global-teams/src/ConnectedGlobalTeams'

// Authentication
import Login from 'editoria-component-login/src/LoginContainer'
import Signup from 'editoria-component-signup/src/SignupContainer'
import PasswordReset from 'pubsweet-component-password-reset-frontend/PasswordReset'

// Editor
import Wax from 'pubsweet-component-wax/src/ConnectedWax'
import WithConfig from 'pubsweet-component-wax/src/WithConfig'

// Editoria
import BookBuilder from 'pubsweet-component-bookbuilder/src/ConnectedBookBuilder'
import Dashboard from 'pubsweet-component-editoria-dashboard/src/ConnectedDashboard'

import PagedStyler from 'pubsweet-component-bookbuilder/src/PagedStyler/PagedStyler'
import Navigation from 'pubsweet-component-editoria-navigation/src/Navigation'
import PrivateRoute from 'pubsweet-component-editoria-navigation/src/PrivateRoute'

import Connected from 'pubsweet-component-editoria-navigation/src/ConnectedNavigation'
import PageLayout from './elements/PageLayout'
import Page from './elements/Page'

// Pass configuration to editor
const Editor = WithConfig(Wax, {
  layout: 'editoria',
  lockWhenEditing: true,
  pollingTimer: 1500,
  autoSave: false,
  menus: {
    topToolBar: 'topDefault',
    sideToolBar: 'sideDefault',
    overlay: 'defaultOverlay',
  },
})
//

const ConnectedNavigation = Connected(Navigation)

const GlobalStyle = createGlobalStyle`
  html {
     height: 100%;
   }

   body {
     height: 100%;
     overflow: hidden;
     #root,
  #root > div {
    height: 100%;
  }
  #root > div > div {
    height: 100%;
  }
 }
`

export default (
  <Fragment>
    <GlobalStyle />
    <Switch>
      <Redirect exact path="/" to="/books" />
      <Route
        path="/login"
        render={props => (
          <Login {...props} logo="/assets/pubsweet-rgb-small.jpg" />
        )}
      />
      <Route
        path="/signup"
        render={props => (
          <Signup {...props} logo="/assets/pubsweet-rgb-small.jpg" />
        )}
      />
      <Route component={PasswordReset} path="/password-reset" />
      <PageLayout>
        <ConnectedNavigation />
        <Page>
          <Switch>
            <PrivateRoute component={Dashboard} exact path="/books" />
            <PrivateRoute
              component={PagedStyler}
              path="/books/:id/pagedPreviewer/paged/:hashed"
            />
            <PrivateRoute
              component={BookBuilder}
              path="/books/:id/book-builder"
            />

            <PrivateRoute
              component={Editor}
              path="/books/:bookId/bookComponents/:bookComponentId"
            />
            <PrivateRoute
              component={Editor}
              exact
              path="/books/:bookId/bookComponents/:bookComponentId/preview"
            />
            {/* <PrivateRoute component={UsersManager} path="/users" /> */}
            <PrivateRoute component={GlobalTeamsManager} path="/globalTeams" />
          </Switch>
        </Page>
      </PageLayout>
    </Switch>
  </Fragment>
)
