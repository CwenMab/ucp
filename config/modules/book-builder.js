module.exports = {
  instance: 'UCP',
  chapter: {
    dropdownValues: {
      back: ['Appendix A', 'Appendix B', 'Appendix C'],
      front: ['Table of Contents', 'Introduction', 'Preface'],
    },
  },
  stages: [
    {
      title: 'Rédaction',
      type: 'edit',
    },
    {
      title: 'Relecture CR',
      type: 'review',
    },
    {
      title: 'Relecture PS',
      type: 'review',
    },
    {
      title: 'Relecture PC',
      type: 'review',
    },
    {
      title: 'Greffe',
      type: 'clean_up',
    },
    {
      title: 'Final',
      type: 'final',
    },
  ],
  divisions: [
    {
      name: 'Body',
      showNumberBeforeComponents: ['chapter'],
      allowedComponentTypes: ['chapter'],
      defaultComponentType: 'chapter',
    },
  ],
  lockTrackChangesWhenReviewing: true,
}
