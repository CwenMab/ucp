sudo apt install vagrant
sudo apt install ansible # v > 2.4

cd env/Vagrant/debian/
vagrant up --provision
vagrant ssh -c "cd /var/www/plume && yarn install && sh scripts/utils/link_custom_lib.sh && source config/development.env && yarn resetdb"
