#!/usr/bin/env bash

cd ../custom_package/wax/packages/wax-core/
yarn link
cd - 
yarn link wax-editor-core

cd ../custom_package/wax/packages/wax-react/
yarn link
cd - 
yarn link wax-editor-react

cd ../custom_package/editoria/packages/dashboard
yarn link
cd -
yarn link pubsweet-component-editoria-dashboard

cd ../custom_package/editoria/packages/navigation
yarn link
cd -
yarn link pubsweet-component-editoria-navigation

cd ../custom_package/editoria/packages/bookbuilder
yarn link
cd -
yarn link pubsweet-component-bookbuilder

cd ../custom_package/editoria/packages/parser
yarn link
cd -
cd ../custom_package/editoria/packages/api
yarn link plume-book-parser

cd ../custom_package/editoria/packages/api
yarn link
yarn install
cd -
yarn link editoria-api

cd ../custom_package/editoria/packages/paged-viewer
yarn link
cd -
yarn link pubsweet-component-pagedjs-viewer
