cd env/Vagrant/centos/
vagrant up --provision
vagrant ssh -c "cd /var/www/plume && yarn install && sh scripts/utils/link_custom_lib.sh && source config/development.env && yarn resetdb"
