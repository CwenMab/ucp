const credentials = {
  admin: {
    username: 'admin',
    password: 'password',
  },
  user1: {
    givenName: 'John Travolta',
    surname: 'actor',
    username: 'user1',
    password: '123456789',
    email: 'john.travolta@awesome.com',
  },
  author: {
    username: 'author',
    password: 'password',
  },
  productionEditor: {
    username: 'productionEditor',
    password: 'password',
  },
  globalProductionEditor: {
    username: 'globalProductionEditor',
    password: 'password',
  },
  copyEditor: {
    username: 'copyEditor',
    password: 'password',
  },
}

module.exports = credentials
