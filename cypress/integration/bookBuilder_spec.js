describe('BookBuilder', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/createBooksWithUsersAndTeams.js')
  })
  it('production editor can navigate to book builder for the book she/he is assigned on', () => {
    cy.login('productionEditor')
    cy.getCollections().then(res => {
      const { body } = res
      const { data } = body
      const { getBookCollections } = data
      const collection = getBookCollections[0]
      const { books } = collection
      const productionEditorBook = books[0]

      cy.visit(`/books/${productionEditorBook.id}/book-builder`)
      cy.contains('BODY')
    })
  })
})
